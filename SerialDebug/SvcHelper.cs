﻿using System;
using System.Collections.Generic;
using System.Linq;
using RestSharp;
using Newtonsoft.Json;

namespace SerialDebug
{
    public class SvcHelper
    {
        /// <summary>
        /// 键{param}
        /// </summary>
        internal const string KEY_PARAM = "param";

        /// <summary>
        /// 客户端对象集合
        /// </summary>
        private static IList<IRestClient> clients = null;

        /// <summary>
        /// 调用Api接口，并返回响应值
        /// </summary>
        /// <typeparam name="T">响应值类型</typeparam>
        /// <param name="pParams">请求参数</param>
        /// <param name="pHeaders">请求头</param>
        /// <param name="pMethodType">方法类型</param>
        /// <returns>返回的响应值</returns>
        public static T InvokeApi<T, K>(string pSvcUrl, Dictionary<string, K> pParams, Dictionary<string, string> pHeaders = null, Method pMethodType = Method.POST)
        {
            if (string.IsNullOrEmpty(pSvcUrl))
                throw new ApplicationException($"服务地址[SvcUrl]未配置");
            if (clients == null)
                clients = new List<IRestClient>();
            IRestClient client = clients.FirstOrDefault(o => o.BaseUrl.OriginalString == pSvcUrl);
            if (client == null)
            {
                client = new RestClient(pSvcUrl)
                {
                    Timeout = -1
                };
                clients.Add(client);
            }
            var req = new RestRequest(pMethodType)
            {
                AlwaysMultipartFormData = true
            };
            if (pHeaders?.Any() ?? false)
                foreach (string headerKey in pHeaders.Keys)
                    req.AddHeader(headerKey, pHeaders[headerKey]);
            if (pParams?.Any() ?? false)
                foreach (string paraKey in pParams.Keys)
                    req.AddParameter(paraKey, typeof(K).Equals(typeof(string)) ? pParams[paraKey]?.ToString() : (pParams[paraKey] == null ? null : JsonConvert.SerializeObject(pParams[paraKey])));
            var res = client.Execute(req);
            T result = default;
            if (!string.IsNullOrEmpty(res.Content))
                result = JsonConvert.DeserializeObject<T>(res.Content.TrimStart('"').TrimEnd('"').Replace("\\", string.Empty));
            return result;
        }

        /// <summary>
        /// 销毁
        /// </summary>
        public static void Dispose()
        {
            if (clients?.Any() ?? false)
                lock (clients)
                    clients.Clear();
        }
    }
}
