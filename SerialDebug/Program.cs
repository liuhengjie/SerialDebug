﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;
using System.IO;
using System.Reflection;
using System.Text;
using System.Linq;
using System.Configuration;
using Newtonsoft.Json;
using System.Threading;

namespace SerialDebug
{
    static class Program
    {
        /// <summary>
        /// 应用程序的主入口点。
        /// </summary>
        [STAThread]
        static void Main()
        {
            try
            {
                //处理未捕获的异常  
                Application.SetUnhandledExceptionMode(UnhandledExceptionMode.CatchException);
                //处理UI线程异常  
                Application.ThreadException += new System.Threading.ThreadExceptionEventHandler(Application_ThreadException);
                //处理非UI线程异常  
                AppDomain.CurrentDomain.UnhandledException += new UnhandledExceptionEventHandler(CurrentDomain_UnhandledException);

                Application.EnableVisualStyles();
                Application.SetCompatibleTextRenderingDefault(false);

                #region Redis

                //Redis最大存储键个数
                decimal REDIS_KEY_MAX_COUNT = (decimal)Math.Pow(2, 32);

                //键{环保数据前缀}
                const string KEY_EPDATA_PREFIX = "SannoEPData";

                //设备编号
                string DeviceId = ConfigurationManager.AppSettings["DeviceId"];

                //需循环标志位，true为需循环
                bool needLoop = true;

                //初始化Redis
                RedisHelper.Init();

                //产生数据间隔(毫秒)
                if (!int.TryParse(ConfigurationManager.AppSettings["CreateDataInterval"], out int createDataInterval))
                    createDataInterval = 5000;
                ////启动产生数据线程
                //ThreadPool.QueueUserWorkItem((state) =>
                //{
                //    while (needLoop)
                //    {
                //        Thread.CurrentThread.Join(createDataInterval);
                //        try
                //        {
                //            RedisHelper.StringSet($"{KEY_EPDATA_PREFIX}_{DeviceId}_{DateTime.Now:yyyyMMddHHmmssfff}_{new Random().Next(1000)}", JsonConvert.SerializeObject(prepareData()));
                //        }
                //        catch
                //        { }
                //    }
                //});

                //计算最大保留天数
                var decValidKeepDay = Math.Truncate(REDIS_KEY_MAX_COUNT / (24 * 60 * 60 * 1000 / createDataInterval));
                var validKeepDay = decValidKeepDay > 90 ? 90 : (int)decValidKeepDay;
                //发送数据间隔(毫秒)
                if (!int.TryParse(ConfigurationManager.AppSettings["SendDataInterval"], out int sendDataInterval))
                    sendDataInterval = 5000;
                //启动发送数据线程
                ThreadPool.QueueUserWorkItem((state) =>
                {
                    while (needLoop)
                    {
                        try
                        {
                            var allKeys = RedisHelper.GetAllKey($"{KEY_EPDATA_PREFIX}_{DeviceId}");
                            if (allKeys?.Any() ?? false)
                            {
                                allKeys.ForEach((key) =>
                                {
                                    try
                                    {
                                        #region 调用API发送数据

                                        string epValue = RedisHelper.StringGet(key);
                                        var strParas = new Dictionary<string, string>
                                        {
                                            {
                                                SvcHelper.KEY_PARAM,
                                                epValue
                                            }
                                        };
                                        var rep = SvcHelper.InvokeApi<Entity.V1.GasData.Response, string>(ConfigurationManager.AppSettings?["ApiSvcUrlGasData"], strParas);
                                        switch (rep?.Code)
                                        {
                                            case (int)Entity.V1.GasData.EnumList.ResponseResultCodeList.Success:
                                                if (!RedisHelper.KeyMove(key, RedisHelper.DBTypeList.WS, RedisHelper.DBTypeList.AS, new TimeSpan(validKeepDay, 0, 0, 0, 0)))
                                                    writeLog($"数据[Key={key}]发送成功，但迁移失败，请检查!");
                                                break;
                                            default:
                                                writeLog($"数据[Key={key}]发送失败，原因：API返回[Code={rep?.Code}, Message={rep?.Message}]");
                                                break;
                                        }

                                        #endregion 调用API发送数据
                                    }
                                    catch (Exception ex)
                                    {
                                        writeLog($"数据[Key={key}]发送失败，原因：{ex.Message}");
                                    }
                                });
                            }
                        }
                        catch (Exception ex)
                        {
                            writeLog($"数据发送失败，原因：{ex.Message}");
                        }
                        Thread.CurrentThread.Join(sendDataInterval);
                    }
                });

                #endregion Redis

                Application.Run(new frmMain());

            }
            catch (Exception ex)
            {
                ExceptionHandler(ex);
            }

        }

        static Entity.V1.GasData.Request prepareData()
        {
            #region 模拟准备数据(需修改)

            var rad = new Random();
            var tags = new List<Entity.V1.GasData.RequestTag>();
            for (int i = 1; i <= 20; i++)
                tags.Add(new Entity.V1.GasData.RequestTag
                {
                    TagIdentifier = $"recyclePumpCurrent{i}",
                    TagData = new List<Entity.V1.GasData.RequestTagData>
                    {
                        new Entity.V1.GasData.RequestTagData
                        {
                            Timestamp = DateTime.Now.ToString("yyyyMMddHHmmss.fff"),
                            Value = rad.Next(5, 20),
                            Result = 0
                        }
                    }
                });
            return new Entity.V1.GasData.Request
            {
                DataVersion = "v1.0",
                Tags = tags
            };

            #endregion 模拟准备数据(需修改)
        }

        static void ExceptionHandler(Exception ex)
        {
           
            try
            {
                if (ex != null)
                {
                    StringBuilder msg = new StringBuilder();
                    msg.AppendFormat("{0}Unhandled Exception:\r\n", DateTime.Now.ToString());
                    string Version = Assembly.GetExecutingAssembly().GetName().Version.ToString();
                    msg.AppendFormat("Version:        {0}\r\n", Version);
                    msg.AppendFormat("Exception Type: {0}\r\n", ex.GetType().Name);
                    msg.AppendFormat("Message:        {0}\r\n", ex.Message);
                    msg.AppendFormat("StackTrace:     {0}\r\n", ex.StackTrace);


                    DialogResult dr = MessageBox.Show("An application error occurred. Please contact the author\r\nWhether to send e-mail", "system exception", MessageBoxButtons.YesNoCancel, MessageBoxIcon.Error);
                    if (dr == DialogResult.Yes)
                    {
                        //writeLog(msg.ToString());

                        string title = string.Format("SerialDebug V{0} exception", Version);
                        string body = msg.ToString();
                        sentEmail("516409354@qq.com", title, msg.ToString());

                    }
                }
                else
                {
                    MessageBox.Show("An application error occurred. Please contact the author", "system exception", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }

            }
            catch (System.Exception e)
            {
                Console.WriteLine(e.ToString());
            }
            


        }


        static void sentEmail(string emailAddress, string Title, string Body)
        {
            string bodyContent;

            bodyContent = Body.Replace("\r", "%0d");
            bodyContent = bodyContent.Replace("\n", "%0a");
            bodyContent = bodyContent.Replace("&", "%26");
            bodyContent = bodyContent.Replace("<","%3c");
            bodyContent = bodyContent.Replace(">", "%3e");
            bodyContent = bodyContent.Replace("\"", "%22");

            string emailMsg = string.Format("mailto:{0}?subject={1}&body={2}",
                emailAddress,
                Title,
                bodyContent);
            System.Diagnostics.Process.Start(emailMsg);

        }
        /// <summary>
        ///这就是我们要在发生未处理异常时处理的方法，我这是写出错详细信息到文本，如出错后弹出一个漂亮的出错提示窗体，给大家做个参考
        ///做法很多，可以是把出错详细信息记录到文本、数据库，发送出错邮件到作者信箱或出错后重新初始化等等
        ///这就是仁者见仁智者见智，大家自己做了。
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        static void Application_ThreadException(object sender, System.Threading.ThreadExceptionEventArgs e)
        {

            Exception ex = e.Exception as Exception;

            ExceptionHandler(ex);

        }
        static void CurrentDomain_UnhandledException(object sender, UnhandledExceptionEventArgs e)
        {
            Exception ex = e.ExceptionObject as Exception;

            ExceptionHandler(ex);

        }

        //删除指定日期文件，保留7天
        private static void DeleteLogs(string dir,string prefix)
        {
            int days = 180;
            //日志保留时长 单位：天
            string logsDay = ConfigurationManager.AppSettings["logsDay"];
            if (!string.IsNullOrEmpty(logsDay))
            {
                days = int.Parse(logsDay);
            }

            try
            {
                if (!Directory.Exists(dir))
                {
                    return;
                }
                var now = DateTime.Now;
                foreach (var f in Directory.GetFileSystemEntries(dir).Where(f => File.Exists(f)))
                {
                    if (f.StartsWith(prefix))
                    {
                        var t = File.GetCreationTime(f);
                        var elapsedTicks = now.Ticks - t.Ticks;
                        var elaspsedSpan = new TimeSpan(elapsedTicks);
                        if (elaspsedSpan.TotalDays > days)
                        {
                            File.Delete(f);
                        }
                    }
                }
            }
            catch (Exception ex)
            {

            }
        }

        static private string today = "";
        /// <summary>
        /// 写文件
        /// </summary>
        /// <param name="str"></param>
        public static void writeLog(string str)
        {
            if (!Directory.Exists(@"Log"))
            {
                Directory.CreateDirectory(@"Log");
            }

            string logdate = DateTime.Now.Date.ToString("yyyyMMdd");
            using (StreamWriter sw = new StreamWriter(@"Log\Log_" + logdate + ".txt", true))
            {
                if(str.EndsWith("\r\n"))
                {
                    sw.Write(str);
                }
                else
                {
                    sw.WriteLine(str);
                }
                sw.Close();
            }

            if (today == "")
            {
                today = logdate;
            }
            else if (today != logdate)
            {
                string rootPath = Directory.GetCurrentDirectory();
                DeleteLogs(rootPath, @"Log\Log_");
                today = logdate;
            }

        }
        public static void writeDataLog(string datastr)
        {
            if (!Directory.Exists(@"DataLog"))
            {
                Directory.CreateDirectory(@"DataLog");
            }
            string logdate = DateTime.Now.Date.ToString("yyyyMMdd");
            using (StreamWriter sw = new StreamWriter(@"DataLog\DataLog_" + logdate + ".csv", true))
            {
                sw.WriteLine(datastr);
                sw.Close();
            }
        }
    }
}