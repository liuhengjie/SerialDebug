namespace SerialDebug.Entity.V1.GasData
{
    /// <summary>
    /// 枚举列表
    /// </summary>
    public class EnumList
    {
        /// <summary>
        /// 请求标签标识数据读取结果列表
        /// </summary>
        public enum RequestTagResultList
        {
            /// <summary>
            /// 正常
            /// </summary>
            Normal = 0,

            /// <summary>
            /// 设备未准备好
            /// </summary>
            NotReady = 128,

            /// <summary>
            /// 参数错误
            /// </summary>
            ParemeterError = 131,

            /// <summary>
            /// 设备状态错误
            /// </summary>
            DeviceStatusError = 132
        }

        /// <summary>
        /// 响应结果编码列表
        /// </summary>
        public enum ResponseResultCodeList
        {
            /// <summary>
            /// 成功
            /// </summary>
            Success = 200,

            /// <summary>
            /// 失败
            /// </summary>
            Failure = 500
        }
    }
}
