using Newtonsoft.Json;
using System.Collections.Generic;

namespace SerialDebug.Entity.V1.GasData
{
    /// <summary>
    /// 请求类标签标识
    /// </summary>
    public class RequestTag
    {
        /// <summary>
        /// 获取或设置标签标识
        /// </summary>
        [JsonProperty("tagIdentifier")]
        public string TagIdentifier { get; set; }

        /// <summary>
        /// 获取或设置标签数据
        /// </summary>
        [JsonProperty("tagData")]
        public IEnumerable<RequestTagData> TagData { get; set; }
    }
}
