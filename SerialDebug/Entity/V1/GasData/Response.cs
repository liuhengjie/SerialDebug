using Newtonsoft.Json;

namespace SerialDebug.Entity.V1.GasData
{
    /// <summary>
    /// 响应类
    /// </summary>
    public class Response
    {
        /// <summary>
        /// 获取或设置结果编码
        /// </summary>
        [JsonProperty("code")]
        public int Code { get; set; }

        /// <summary>
        /// 获取或设置结果消息
        /// </summary>
        [JsonProperty("message")]
        public string Message { get; set; }
    }
}
