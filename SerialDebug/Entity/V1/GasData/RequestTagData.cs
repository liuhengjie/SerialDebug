﻿using Newtonsoft.Json;

namespace SerialDebug.Entity.V1.GasData
{
    /// <summary>
    /// 请求类标签标识
    /// </summary>
    public class RequestTagData
    {
        /// <summary>
        /// 获取采样时的时间戳
        /// </summary>
        [JsonProperty("timestamp")]
        public string Timestamp { get; set; }

        /// <summary>
        /// 获取或设置采样值
        /// </summary>
        [JsonProperty("value")]
        public decimal? Value { get; set; }

        /// <summary>
        /// 获取或设置数据读取结果
        /// </summary>
        [JsonProperty("result")]
        public EnumList.RequestTagResultList Result { get; set; }
    }
}
