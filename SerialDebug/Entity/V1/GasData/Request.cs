using System.Collections.Generic;
using Newtonsoft.Json;

namespace SerialDebug.Entity.V1.GasData
{
    /// <summary>
    /// 请求类
    /// </summary>
    public class Request
    {
        /// <summary>
        /// 获取数据版本
        /// </summary>
        [JsonProperty("dataVersion")]
        public string DataVersion { get; set; }

        /// <summary>
        /// 获取或设置标签标识
        /// </summary>
        [JsonProperty("tags")]
        public List<RequestTag> Tags { get; set; }
    }
}
