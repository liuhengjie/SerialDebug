﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using StackExchange.Redis;

namespace SerialDebug
{
    public static class RedisHelper
    {
        /// <summary>
        /// 数据库类型列表
        /// </summary>
        public enum DBTypeList
        {
            /// <summary>
            /// 待发
            /// </summary>
            WS = 0,

            /// <summary>
            /// 已发
            /// </summary>
            AS = 1
        }

        /// <summary>
        /// 数据库实体类
        /// </summary>
        public sealed class DBEntity
        {
            /// <summary>
            /// 获取数据库类型
            /// </summary>
            public DBTypeList DBType
            { get; private set; }

            /// <summary>
            /// 获取数据库索引号
            /// </summary>
            public int Index
            { get; private set; }

            /// <summary>
            /// 获取数据库对象
            /// </summary>
            public IDatabase DB
            { get; private set; }

            /// <summary>
            /// 构造函数
            /// </summary>
            /// <param name="pDBType">数据库类型</param>
            /// <param name="pIndex">数据库索引号</param>
            public DBEntity(DBTypeList pDBType, int pIndex)
            {
                if (!(cmRedis?.IsConnected ?? false))
                    throw new ApplicationException($"Redis服务器尚未连接");
                if (pIndex < 0 || pIndex > 15)
                    throw new ApplicationException($"Redis数据库索引{pIndex}错误，值范围[0～15]");
                DBType = pDBType;
                Index = pIndex;
                DB = cmRedis.GetDatabase(Index);
            }
        }

        /// <summary>
        /// 已初始化标志位，true为已初始化
        /// </summary>
        private static bool isInit = false;

        /// <summary>
        /// Redis服务器连接管理器
        /// </summary>
        private static IConnectionMultiplexer cmRedis = null;

        /// <summary>
        /// Redis数据库集合
        /// </summary>
        private static List<DBEntity> dbs = null;

        /// <summary>
        /// Redis数据库是否连接中
        /// </summary>
        private static readonly RedisKey RK_CONN = new RedisKey("DBIsConnected");

        /// <summary>
        /// 初始化
        /// </summary>
        public static void Init()
        {
            if (!isInit)
            {
                string redisSvrAddr = ConfigurationManager.AppSettings?["RedisSvrAddr"];
                if (string.IsNullOrEmpty(redisSvrAddr))
                    throw new ApplicationException($"Redis服务地址(RedisSvrAddr)不允许为空");
                if (!int.TryParse(ConfigurationManager.AppSettings?["RedisDBIndexWS"], out int redisDBIndexWS))
                    throw new ApplicationException($"Redis数据库索引(待发)(RedisDBIndexWS)设置错误，必须为数值");
                if (!int.TryParse(ConfigurationManager.AppSettings?["RedisDBIndexAS"], out int redisDBIndexAS))
                    throw new ApplicationException($"Redis数据库索引(已发)(RedisDBIndexAS)设置错误，必须为数值");
                var config = new ConfigurationOptions
                {
                    AllowAdmin = true,
                };
                config.EndPoints.Add(redisSvrAddr);
                cmRedis = ConnectionMultiplexer.Connect(config);
                if (dbs == null)
                    dbs = new List<DBEntity>();
                dbs.Clear();
                dbs.AddRange(new DBEntity[] { new DBEntity(DBTypeList.WS, redisDBIndexWS), new DBEntity(DBTypeList.AS, redisDBIndexAS)});
                isInit = true;
            }
        }

        /// <summary>
        /// 销毁
        /// </summary>
        public static void Dispose()
        {
            if (isInit)
            {
                cmRedis?.Close(true);
                isInit = false;
            }
        }

        /// <summary>
        /// 检查数据库是否存活，true为存活
        /// </summary>
        /// <param name="pDBType">数据库类型</param>
        private static void checkDBAlive(DBTypeList? pDBType = DBTypeList.WS)
        {
            if (pDBType.HasValue)
                checkDBAlive(new List<DBTypeList> { pDBType.Value });
        }

        /// <summary>
        /// 检查数据库是否存活，true为存活
        /// </summary>
        /// <param name="pDBTypes">数据库类型集合</param>
        private static void checkDBAlive(List<DBTypeList> pDBTypes)
        {
            if (!isInit)
                throw new ApplicationException("请先初始化");
            if (pDBTypes?.Any() ?? false)
                pDBTypes.ForEach(dbType => 
                {
                    if(!getDBEntity(dbType).DB.IsConnected(RK_CONN, CommandFlags.DemandMaster))
                        throw new ApplicationException($"数据库{dbType}连接断开，请检查");
                });
        }

        /// <summary>
        /// 获取数据库实体对象
        /// </summary>
        /// <param name="pDBType">数据库类型，默认为待发库</param>
        /// <returns>数据库实体对象</returns>
        private static DBEntity getDBEntity(DBTypeList pDBType = DBTypeList.WS)
        {
            return dbs.FirstOrDefault(o => o.DBType == pDBType);
        }

        /// <summary>
        /// 保存键
        /// </summary>
        /// <param name="pKey">键名</param>
        /// <param name="pValue">键值</param>
        /// <param name="pExpiry">有效期</param>
        /// <param name="pDBType">数据库类型，默认为待发库</param>
        public static bool StringSet(string pKey, string pValue, TimeSpan? pExpiry = null, DBTypeList pDBType = DBTypeList.WS)
        {
            checkDBAlive(pDBType);
            return getDBEntity(pDBType).DB.StringSet(new RedisKey(pKey), new RedisValue(pValue), pExpiry);
        }

        /// <summary>
        /// 获取键
        /// </summary>
        /// <param name="pKey">键名</param>
        /// <param name="pDBType">数据库类型，默认为待发库</param>
        public static string StringGet(string pKey, DBTypeList pDBType = DBTypeList.WS)
        {
            checkDBAlive(pDBType);
            return getDBEntity(pDBType).DB.StringGet(new RedisKey(pKey));
        }

        /// <summary>
        /// 移动键
        /// </summary>
        /// <param name="pKey">键名</param>
        /// <param name="pSrcDBType">目标数据库类型，默认为待发库</param>
        /// <param name="pDesDBType">目标数据库类型，默认为已发库</param>
        /// <param name="pExpiry">有效期</param>
        public static bool KeyMove(string pKey, DBTypeList pSrcDBType = DBTypeList.WS, DBTypeList pDesDBType = DBTypeList.AS, TimeSpan? pExpiry = null)
        {
            checkDBAlive(pSrcDBType);
            var rkKey = new RedisKey(pKey);
            bool result = getDBEntity(pSrcDBType).DB.KeyMove(rkKey, getDBEntity(pDesDBType).Index);
            if (result && pExpiry != null)
                getDBEntity(pSrcDBType).DB.KeyExpire(rkKey, pExpiry);
            return result;
        }

        /// <summary>
        /// 删除键
        /// </summary>
        /// <param name="pKey">键名</param>
        /// <param name="pValue">键值</param>
        /// <param name="pDBType">数据库类型，默认为待发库</param>
        /// <param name="pExpiry"></param>
        public static bool KeyDelete(string pKey, DBTypeList pDBType = DBTypeList.WS)
        {
            checkDBAlive(pDBType);
            return getDBEntity(pDBType).DB.KeyDelete(new RedisKey(pKey));
        }

        /// <summary>
        /// 获取所有键名
        /// </summary>
        /// <param name="pPrefixKey">键名前缀</param>
        /// <param name="pDBType">数据库类型，默认为待发库</param>
        public static List<string> GetAllKey(string pPrefixKey, DBTypeList pDBType = DBTypeList.WS)
        {
            var dbPoints = cmRedis.GetEndPoints();
            List<string> allKeys = null;
            if (dbPoints?.Any() ?? false)
                allKeys = new List<string>();
            foreach (var dbPoint in dbPoints)
            {
                var keys = cmRedis.GetServer(dbPoint).Keys(getDBEntity(pDBType).Index, $"{pPrefixKey}*")?.ToList();
                if (keys?.Any() ?? false)
                    allKeys.AddRange(keys.Select(o => o.ToString()));
            }
            return allKeys;
        }
    }
}
